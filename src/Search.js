import React, {useEffect, useState} from 'react'
import { Link } from 'react-router-dom'
import {queryItems} from './api/pushshift'
import { useQuery } from 'react-query'
import useForm from 'react-hook-form'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components/macro'
import Result from './Result'




// PLACES PARAMS ARE USED:
//    (* means automatically filled, based on validUrlParams)

// * getUrlParams body
// * updateURLParams signature
// * updateURLParams body
// form (manual)
// api.queryItems signature

const validUrlParams = {'q': ''}
const search_example_1 = "(fun or boring) author:a"
const search_example_2 = "good news author:a"
const search_example_3 = `"good news" author:a`
const search_example_4 = `"good news" or author:a`
const search_example_5 = `"thank you" (a:gab or a:a)`
const search_example_6 = `account.followers_count:>10000`


const getUrlParams = () => {
  const searchParams = new URLSearchParams(window.location.search)
  const result = {}
  for (var p in validUrlParams) {
    const val = searchParams.get(p)
    if (val) result[p] = val
  }
  return result
}

const getUrlParamsString = () => {
  const params = new URLSearchParams(getUrlParams())
  return params.toString()
}

const updateURLParams = (object, history) => {
  const origQueryString = new URLSearchParams(window.location.search).toString()
  const newSearchParams = new URLSearchParams()
  for (var p in validUrlParams) {
    if (object[p]) {
      newSearchParams.set(p, object[p])
    }
  }
  const newQueryString = newSearchParams.toString()
  if (origQueryString !== newQueryString) {
    const newURL = window.location.pathname + '?' + newQueryString
    history.push(newURL)
  }
}

const Search = () => {
  const history = useHistory()
  const searchParams = getUrlParams()
  const [show, setShow] = useState(false)
  const { register, reset, handleSubmit } = useForm({
    defaultValues: searchParams
  })
  //to add more params, use getUrlParamsString, then inside useEffect, parse parameters
  useEffect(() => {
    reset({q: searchParams.q})
  }, [searchParams.q, reset]);

  const { data, isLoading, error } = useQuery(
    getUrlParamsString() && [getUrlParamsString(), searchParams],
    queryItems,
    {retry: false}
  )

  const onSearch = (data) => {
    updateURLParams(data, history)
  }
  return (
    <>
      <h3>Search</h3>
      <p>Search is strict, following Elastic's full {' '}
        <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#query-string-syntax">query string syntax</a>
        . Additional search fields may be found in <a href="https://gab.pushshift.io/_mapping">Pushshift's mapping.</a>
      </p>
        {show ?
          <>
            <p onClick={() => setShow(false)}>[–] hide examples</p>
            <ul>
              <li><code><Link to={`?q=${search_example_1}`}>{search_example_1}</Link></code>
                <ul><li>search for posts by author "a" containing fun or boring</li></ul></li>
              <li><code><Link to={`?q=${search_example_2}`}>{search_example_2}</Link></code>
                <ul><li>search for posts by author "a" containing the words good AND news</li></ul></li>
              <li><code><Link to={`?q=${search_example_3}`}>{search_example_3}</Link></code>
                <ul><li>search for posts by author "a" containing the phrase "good news"</li></ul></li>
              <li><code><Link to={`?q=${search_example_4}`}>{search_example_4}</Link></code>
                <ul><li>search for posts by author "a" OR containing the phrase "good news"</li></ul></li>
              <li><code><Link to={`?q=${search_example_5}`}>{search_example_5}</Link></code>
                <ul><li>search for posts by author "gab" OR author "a" containing the phrase "thank you"</li></ul></li>
              <li><code><Link to={`?q=${search_example_6}`}>{search_example_6}</Link></code>
                <ul><li>search for posts by authors with more than 10,000 followers</li></ul></li>
            </ul>
          </>
        :
          <p onClick={() => setShow(true)}>[+] show examples</p>
        }
      <form onSubmit={handleSubmit(onSearch)}>
        <SearchBox>
          <Input name="q" type='text' ref={register}/>
          <Button type="submit" className='ui-submit'>Search</Button>
        </SearchBox>
      </form>
      {isLoading && <div>Loading</div>}
      {error && <div>Error: {error.message}. Possible issues:
        <ul>
          <li>Query is not formatted properly</li>
          <li>Database is down</li>
        </ul>
      </div>}
      {data && (
        data.map(post => {
          return (
            <Result post={post} key={post.id}/>
          )
        })
      )}
    </>
  )
}




const SearchBox = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: stretch;
  font-family: "Roboto", sans-serif;


`

const Input = styled.input`
  border-radius: 4px;
  border: 1px solid #ccc;
  padding: 16px;
  outline: none;
  position: relative;
  font-size: 14px;
  width: 100%;
  :focus {
    box-shadow: rgba(59, 69, 79, 0.3) 0px 2px 4px;
    border-top: 1px solid #3a56e4;
    border-left: 1px solid #3a56e4;
    border-right: 1px solid #3a56e4;
    border-bottom: 1px solid #3a56e4;
  }
`

const Button = styled.button`
  font-size: 14px;
  padding: 16px;
  margin-left: 10px;
  text-shadow: rgba(0, 0, 0, 0.05) 0px 1px 2px;
  color: white;
  border: none;
  box-shadow: rgba(0, 0, 0, 0.05) 0px 0px 0px 1px inset, rgba(59, 69, 79, 0.05) 0px 1px 0px;
  background: linear-gradient(#2da0fa, #3158ee) #2f7cf4;
  cursor: pointer;
  border-radius: 4px;
  :hover {
    box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 0px 1px inset, rgba(59, 69, 79, 0.3) 0px 2px 4px;
    background: linear-gradient(#3cabff, #4063f0) #3d84f7;
  }
`


export default Search
