const API_URL = 'https://gab.pushshift.io/search'

const mapKeywordsToFields = {
  'author': 'account.acct.keyword',
  'a': 'account.acct.keyword'
}

const keywordSymbol = ':'

const replaceAll = (str, search, replace) => {
  return str.split(search).join(replace)
}

const reformatQuery = (q) => {
  var result = q
  for (const keyword in mapKeywordsToFields) {
    result = replaceAll(result, keyword+keywordSymbol, mapKeywordsToFields[keyword]+keywordSymbol)
  }
  result = replaceAll(result, ' or ', ' OR ')
  return result
}

export const queryItems = ({q='', size = 500, sort = 'created_at:desc'}) => {
  const queryParams = {}
  if (q) queryParams.q = reformatQuery(q)
  if (size) queryParams.size = size
  if (sort) queryParams.sort = sort
  queryParams.default_operator = 'AND'

  return window.fetch(API_URL+getQueryString(queryParams))
  .then(response => response.json())
  .then(data => {
    return data.hits.hits.map( item => {
      return item._source
    })
  })
}

const getQueryString = (queryParams) => {
  let queryVals = []
  for (var key in queryParams) {
      queryVals.push(key+'='+queryParams[key])
  }
  return '?'+queryVals.join('&')
}
