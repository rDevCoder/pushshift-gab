import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Search from './Search'
import About from './About'
import { ReactQueryConfigProvider } from 'react-query'
import styled from 'styled-components/macro'
import { createGlobalStyle } from 'styled-components'

const queryConfig = { refetchAllOnWindowFocus: false }

const Body = styled.div`
  width: 800px;
  margin: 0 auto;
  a {
    text-decoration: none;
  }
`
const GlobalStyle = createGlobalStyle`
  @media screen and (min-width: 960px) {
    html {
        margin-left: calc(100vw - 100%);
        margin-right: 0;
    }
  }
`

export default function App() {
  return (
    <ReactQueryConfigProvider config={queryConfig}>
      <Router>
        <GlobalStyle/>
        <Body>
          <ul>
            <li>
              <Link to="/">Search</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
          <hr />
          <Switch>
            <Route exact path="/">
              <Search />
            </Route>
            <Route path="/about">
              <About />
            </Route>
          </Switch>
        </Body>
      </Router>
    </ReactQueryConfigProvider>
  );
}
