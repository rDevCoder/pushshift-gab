import React, {useState} from 'react'
import styled from 'styled-components/macro'


const Result = (props) => {
  const [showRaw, setShowRaw] = useState(false)
  const {post} = props

  return (
    <StyledResult>
      <h2><a href={`https://gab.com/${post.account.acct}`}>{post.account.display_name || post.account.acct}</a></h2>
      <p>{post.body}</p>
      <a href={post.url}>link to post</a>
      <StyledTime time={post.created_at}/>
      {showRaw ?
        <>
          <div onClick={() => setShowRaw(false)}>[–] raw data</div>
          <pre>{JSON.stringify(post, null, 2)}</pre>
        </>
        :
        <div onClick={() => setShowRaw(true)}>[+] raw data</div>
      }
    </StyledResult>
  )
}

const StyledResult = styled.div`
  margin-top: 32px;
  padding: 24px 12px;
  border: 1px solid #f0f0f0;
  border-radius: 4px;

  pre {
    white-space: pre-wrap;       /* Since CSS 2.1 */
    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
  }
`

const Time = (props) => {
  const fullDate = new Date(props.time).toString()
  return <div className={props.className} title={fullDate}>{fullDate}</div>
}

const StyledTime = styled(Time)`
  text-align: right;
`


export default Result
