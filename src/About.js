import React from 'react'



const About = () => {
  return (
    <>
      <h3>About</h3>
      <p>Source code: <a href="https://gitlab.com/rDevCoder/pushshift-gab">pushshift-gab</a></p>
    </>
  )
}
export default About
